package it.pegaso.tvRest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TvRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TvRestApplication.class, args);
	}

}
