package it.pegaso.tvRest.repo;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import it.pegaso.tvRest.models.Programma;

public interface ProgrammaRepo extends JpaRepositoryImplementation<Programma, Integer>{

}
