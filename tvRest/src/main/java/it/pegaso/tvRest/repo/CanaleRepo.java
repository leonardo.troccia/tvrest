package it.pegaso.tvRest.repo;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import it.pegaso.tvRest.models.Canale;

public interface CanaleRepo extends JpaRepositoryImplementation<Canale, Integer>{

	
}
