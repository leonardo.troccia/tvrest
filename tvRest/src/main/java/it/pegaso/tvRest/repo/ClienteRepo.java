package it.pegaso.tvRest.repo;

import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;

import it.pegaso.tvRest.models.Cliente;

public interface ClienteRepo extends JpaRepositoryImplementation<Cliente, Integer>{

}
